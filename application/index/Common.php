<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/7
 * Time: 12:25
 */

namespace app\index;


use app\index\model\Login;
use think\Cache;
use think\Cookie;
use think\Loader;
use think\Session;

class Common
{
    public static function arr2show($arr=[]){
        foreach ($arr as $value){
            echo $value.PHP_EOL;
        }
    }
    public static function getToken(){
        $request = \think\Request::instance();
        return $request ->token('__token__','sha1');
    }
    public static function arrayreturn($methed=false,$msg=''){
        $res['result'] =$methed;
        $res['msg'] =$msg;
        return $res;
    }

    public function auth(){
        $cookie = Cookie::get('PHPSESSIsD');
        $session = isset($cookie)?Session::get($cookie):NULL;
        return !$session&&!isset($cookie);
    }
    public static function random_code($length = 6){
    $min = pow(10 , ($length - 1));
    $max = pow(10, $length) - 1;
    return rand($min, $max);
    }
    /**
     * 检测手机短信验证码
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     * @param $mobile
     * @param bool|false $code
     * @return bool
     */
    protected function checkRegSms($mobile, $code = false)
    {
        if (!$mobile) return false;
        if ($code === false) {   //判断60秒以内是否重复发送
            if (!Cache::has('sms_' . $mobile)) return true;
            if (Cache::get('sms_' . $mobile)['times'] > time()) {
                return false;
            } else {
                return true;
            }
        } else {  //判断验证码是否输入正确
            if (!Cache::has('sms_' . $mobile)) return false;
            if (Cache::get('sms_' . $mobile)['code'] == $code) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 设置手机短息验证码缓存
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     * @param $data_cache
     */
    protected function setRegSmsCache($data_cache)
    {
        Cache::set('sms_' . $data_cache['mobile'], 300);
    }

}